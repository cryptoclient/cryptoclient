﻿using Math.Gmp.Native;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRsaClient
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {

                int id = 0;

                MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);
                connexion.Open();
                //on ouvre la connexion
                MySqlCommand command2 = new MySqlCommand("SELECT ID_Profil from Messages M where Sujet ='" + listBox1.SelectedItem.ToString() + "'");
                command2.Connection = connexion;
                MySqlDataReader reader2 = command2.ExecuteReader();


                while (reader2.Read())
                {
                    id = int.Parse(reader2.GetValue(0).ToString());
                }
                connexion.Close();

                connexion.Open();

                MySqlCommand command = new MySqlCommand("SELECT Message, Prive from Messages M inner join Profils P on P.ID = M.ID_Profil  inner join Cles C on M.ID_Destinataire = C.ID_Profil where M.ID_Profil ='" + id + "' and ID_Destinataire ='" + Form2.currentId + "' and Sujet ='" + listBox1.SelectedItem.ToString() + "'");
                command.Connection = connexion;


                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données
                string fullmessage = "";
                string privatekey = "";

                while (reader.Read())
                {
                    fullmessage = reader.GetValue(0).ToString();
                    privatekey = reader.GetValue(1).ToString();
                }

                //on ferme la connexion

                connexion.Close();

                // decryptage
                string[] todecrypte = fullmessage.Split(';');
                mpz_t d = new mpz_t();
                gmp_lib.mpz_init(d);
                Function.setMpz_t(d, privatekey.Split(',')[0]);

                mpz_t n = new mpz_t();
                gmp_lib.mpz_init(n);
                Function.setMpz_t(n, privatekey.Split(',')[1]);

                string asciimessage = Function.decryption(todecrypte, d, n);
                string decryptedmessage = Function.asciiToMsg(asciimessage);
                byte[] bytes = Encoding.Default.GetBytes(decryptedmessage);
                decryptedmessage = Encoding.UTF8.GetString(bytes);
                richTextBox1.AppendText(decryptedmessage);
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un message à décrypter.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                int id = 0;


                MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);
                connexion.Open();
                //on ouvre la connexion
                MySqlCommand command2 = new MySqlCommand("SELECT ID_Profil from Messages M  where Sujet ='" + listBox1.SelectedItem.ToString() + "'");
                command2.Connection = connexion;
                MySqlDataReader reader2 = command2.ExecuteReader();

                while (reader2.Read())
                {
                    id = int.Parse(reader2.GetValue(0).ToString());
                }
                connexion.Close();

                connexion.Open();

                MySqlCommand command = new MySqlCommand("SELECT Message from Messages M inner join Profils P on P.ID = M.ID_Profil where ID_Profil ='" + id + "' and ID_Destinataire ='" + Form2.currentId + "'");
                command.Connection = connexion;


                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données

                while (reader.Read())
                {
                    richTextBox1.AppendText(reader.GetValue(0).ToString());
                }

                //on ferme la connexion

                connexion.Close();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un message.");
            }
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

            //on ouvre la connexion

            connexion.Open();

            MySqlCommand command = new MySqlCommand("SELECT sujet from Messages M where ID_Destinataire ='"+ Form2.currentId +"'");
            command.Connection = connexion;


            MySqlDataReader reader = command.ExecuteReader();

            //on boucle sur les données

            while (reader.Read())
            {
                listBox1.Items.Add(reader.GetValue(0).ToString());
            }

            //on ferme la connexion

            connexion.Close();
        }
    }
}
