﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRsaClient
{
    public partial class Form2 : Form
    {
        public static int currentId { get; set; }
        public Form2()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox2.Text) && !string.IsNullOrEmpty(textBox1.Text))
            {
                bool connected = false;

                MySqlConnection conn = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

                conn.Open();

                MySqlCommand command = new MySqlCommand("SELECT ID, Nom, Prenom from Profils where Adresse_Mail='" + textBox2.Text.ToLower() + "' and password='" + textBox1.Text + "'");
                command.Connection = conn;

                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données

                while (reader.Read())
                {
                    currentId = (int)reader.GetValue(0);
                    if (!string.IsNullOrEmpty(reader.GetValue(1).ToString()))
                        connected = true;
                }

                conn.Close();
                
                if (connected)
                {
                    MessageBox.Show("Vous êtes connecté(e).");
                    this.Hide();
                    this.FormClosed += new FormClosedEventHandler(this.Form2_FormClosing);
                    this.Close();
                }
                else
                    MessageBox.Show("Adresse mail ou mot de passe incorrect.");
            }
            else
            {
                MessageBox.Show("Veuillez remplir tous les champs.");
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            //s'inscrire
            System.Diagnostics.Process.Start("https://algorithmersa.000webhostapp.com/RSAClient2/index.php");
        }

        private void label3_Click(object sender, EventArgs e)
        {
            //mdp oubli"
            System.Diagnostics.Process.Start("https://algorithmersa.000webhostapp.com/RSAClient2/index.php");
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_FormClosing(object sender, FormClosedEventArgs e)
        {
            Form1 home = new Form1();
            home.ShowDialog();
        }
    }
}
