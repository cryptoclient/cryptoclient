﻿using Math.Gmp.Native;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRsaClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<string[]> datas = new List<string[]>();
            MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

            //on ouvre la connexion

            connexion.Open();

            MySqlCommand command = new MySqlCommand("SELECT Nom, Prenom from Profils where ID in (select ID_Destinataire from Destinataire where ID_Profil ='"+ Form2.currentId +"')");
            command.Connection = connexion;


            MySqlDataReader reader = command.ExecuteReader();

            //on boucle sur les données

            while (reader.Read())
            {
                string[] data = new string[] {
                        reader.GetValue(0).ToString(),
                        reader.GetValue(1).ToString()
                    };
                datas.Add(data);
            }
            listBox1.Items.Clear();
            foreach (var line in datas)
            {
                listBox1.Items.Add(line[0] + " " + line[1]);
            }
            connexion.Close();
            connexion.Open();
            MySqlCommand command2 = new MySqlCommand("SELECT Nom, Prenom, IFNULL(MessagesMax,0), Date_Forfait, Libelle, IFNULL(COUNT(M.ID),0)" +
                " from Profils P left join Messages M on P.ID = M.ID_Destinataire inner join Forfait F on P.ID_Forfait = F.ID" +
                " where P.ID = '"+Form2.currentId+ "'"+// and M.Date > (select  DATE_ADD(LAST_DAY(NOW()), INTERVAL -1 MONTH)) and M2.Date > (select  DATE_ADD(LAST_DAY(NOW()), INTERVAL -1 MONTH))" +
                " group by Nom, Prenom, MessagesMax, Date_Forfait, Libelle");
            command2.Connection = connexion;

            MySqlDataReader reader2 = command2.ExecuteReader();

            string user = "";
            string forfait = "";
            DateTime date = DateTime.Now;
            int msgleft = 0;
            int msgreceived = 0;
            int msgmax = 0;
            while (reader2.Read())
            {
                user = reader2.GetValue(0) + " " + reader2.GetValue(1);
                forfait = reader2.GetValue(4).ToString();
                date = (DateTime)reader2.GetValue(3);
                msgmax = int.Parse(reader2.GetValue(2).ToString());
                msgreceived = int.Parse(reader2.GetValue(5).ToString());
            }

            label9.Text = user;
            label6.Text = forfait;
            label8.Text = date.ToShortDateString();

            connexion.Close();
            connexion.Open();
            MySqlCommand cmd = new MySqlCommand("Select IFNULL(count(M.ID),0) from Messages M where M.ID_Profil ="+Form2.currentId);
            cmd.Connection = connexion;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                if (msgmax != 0)
                    msgleft = msgmax - int.Parse(rdr.GetValue(0).ToString());
            }
            label3.Text = msgleft == 0 ? "Illimité" : msgleft.ToString();
            label11.Text = msgreceived.ToString();
            connexion.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 sendmessage = new Form3(listBox1.Items);
            sendmessage.FormClosing += new FormClosingEventHandler(this.Form3_FormClosing);
            sendmessage.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form4 messagereceived = new Form4();
            messagereceived.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Form5 addusers = new Form5(listBox1);
            addusers.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                string user = listBox1.Items[listBox1.SelectedIndex].ToString();
                string prenom = user.Split(' ')[1];
                string nom = user.Split(' ')[0];
                int userid = 0;

                MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

                //on ouvre la connexion

                connexion.Open();
                MySqlCommand command = new MySqlCommand("SELECT ID from Profils where Prenom ='" + prenom + "' and Nom='" + nom + "'");

                command.Connection = connexion;

                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données

                while (reader.Read())
                {
                    userid = (int)reader.GetValue(0);
                }

                connexion.Close();
                connexion.Open();

                MySqlCommand command2 = new MySqlCommand("Delete from Destinataire Where ID_Profil = '" + Form2.currentId + "' and ID_Destinataire ='" + userid + "'");
                command2.Connection = connexion;

                int result = command2.ExecuteNonQuery();
                if (result > 0)
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                connexion.Close();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un utilisateur.");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs evt)
        {
            bool alreadyGenerated = false;
            MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);
            connexion.Open();
            MySqlCommand cmd = new MySqlCommand("Select 1 from Cles where ID_Profil ="+Form2.currentId);
            cmd.Connection = connexion;
            MySqlDataReader rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                if (rdr.HasRows)
                    alreadyGenerated = true;
            }
            connexion.Close();
            if (!alreadyGenerated)
            {
                mpz_t p = new mpz_t();
                gmp_lib.mpz_init(p);
                Function.generateBigPrimeNumber(p);

                mpz_t q = new mpz_t();
                gmp_lib.mpz_init(q);
                Function.generateBigPrimeNumber(q);

                mpz_t n = new mpz_t();
                gmp_lib.mpz_init(n);
                gmp_lib.mpz_mul(n, p, q);

                mpz_t phi_n = new mpz_t();
                gmp_lib.mpz_init(phi_n);

                mpz_t p_one = new mpz_t();
                gmp_lib.mpz_init(p_one);

                mpz_t q_one = new mpz_t();
                gmp_lib.mpz_init(q_one);

                gmp_lib.mpz_sub_ui(p_one, p, 1);
                gmp_lib.mpz_sub_ui(q_one, q, 1);

                gmp_lib.mpz_mul(phi_n, p_one, q_one);

                mpz_t e = new mpz_t();
                gmp_lib.mpz_init(e);
                e = Function.generateBigPrimeNumber2(e);
                //gmp_lib.mpz_init_set_si(e, 67757);

                while (!Function.isPrimeWith(e, phi_n))
                {
                    e = Function.generateBigPrimeNumber2(e);
                }

                mpz_t d = new mpz_t();
                gmp_lib.mpz_init(d);
                gmp_lib.mpz_invert(d, e, phi_n);
                string publickey = e.ToString() + "," + n.ToString();
                string privatekey = d.ToString() + "," + n.ToString();


                //on ouvre la connexion

                connexion.Open();
                MySqlCommand command = new MySqlCommand("insert into Cles (ID_Profil,Public,Prive) values ('" + Form2.currentId + "','" + publickey + "','" + privatekey + "')");
                command.Connection = connexion;
                int a = command.ExecuteNonQuery();


                connexion.Close();

                MessageBox.Show("Les clés publique et privé ont été générées et enregristrées en base. Seule la clé publique sera diffusée.");
            }
            else
            {
                MessageBox.Show("Les clés ont déjà été générées.");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://algorithmersa.000webhostapp.com/RSAClient2/index.php");
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.OnLoad(e);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }
    }
}