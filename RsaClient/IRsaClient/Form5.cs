﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRsaClient
{
    public partial class Form5 : Form
    {
        public ListBox lb { get; set; }

        public Form5(ListBox homelistbox)
        {
            lb = homelistbox;
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text.Length > 2)
            {
                List<string[]> datas = new List<string[]>();
                
                MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

                //on ouvre la connexion

                connexion.Open();

                MySqlCommand command = new MySqlCommand("SELECT Nom, Prenom from Profils where Nom LIKE '%" + textBox1.Text + "%' OR Prenom LIKE '%" + textBox1.Text + "%'");
                command.Connection = connexion;


                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données

                while (reader.Read())
                {
                    string[] data = new string[] {
                        reader.GetValue(0).ToString(),
                        reader.GetValue(1).ToString()
                    };
                    datas.Add(data);
                }
                listBox1.Items.Clear();
                foreach(var line in datas)
                {
                    listBox1.Items.Add(line[0] + " " + line[1]);
                }
                connexion.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                lb.Items.Add(listBox1.SelectedItem);
                int id = 0;
                string nom = listBox1.SelectedItem.ToString().Split(' ')[0];
                string prenom = listBox1.SelectedItem.ToString().Split(' ')[1];

                MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);

                //on ouvre la connexion

                connexion.Open();
                MySqlCommand command = new MySqlCommand("select ID from Profils where Nom = '" + nom + "' and Prenom = '" + prenom + "'");
                command.Connection = connexion;
                MySqlDataReader reader = command.ExecuteReader();

                //on boucle sur les données

                while (reader.Read())
                {
                    id = (int)reader.GetValue(0);
                }
                connexion.Close();
                connexion.Open();
                MySqlCommand command2 = new MySqlCommand("INSERT INTO Destinataire (ID_Profil,ID_Destinataire) values ('" + Form2.currentId + "','" + id + "')");
                command2.Connection = connexion;

                int a = command2.ExecuteNonQuery();
                connexion.Close();
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un utilisateur.");
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}
