﻿using Math.Gmp.Native;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRsaClient
{
    public partial class Form3 : Form
    {
        private ListBox.ObjectCollection items;

        public Form3(ListBox.ObjectCollection items)
        {
            this.items = items;
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }


        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files | *.txt"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                richTextBox1.AppendText(string.Join("\n", System.IO.File.ReadAllLines(dialog.FileName, new UTF7Encoding())));
                richTextBox1.AppendText("\n");
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            checkedListBox1.Items.AddRange(items); 
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs evt)
        {
            if (checkedListBox1.SelectedItems != null && !string.IsNullOrEmpty(textBox1.Text) && !string.IsNullOrEmpty(richTextBox1.Text))
            {
                foreach (object selectedItem in checkedListBox1.CheckedItems)
                {
                    string[] lines = richTextBox1.Lines;
                    string[] words = lines[0].Split(' ');
                    // cryptage

                    MySqlConnection connexion = new MySqlConnection(@ConfigurationManager.AppSettings["connectionString"]);
                    
                    string user = selectedItem.ToString();
                    string nom = "";
                    string prenom = "";
                    
                    nom = user.Split(' ')[0];
                    prenom = user.Split(' ')[1];

                    int userid = 0;

                    //on ouvre la connexion

                    connexion.Open();
                    MySqlCommand command = new MySqlCommand("SELECT ID from Profils where Prenom ='" + prenom + "' and Nom='" + nom + "'");

                    command.Connection = connexion;

                    MySqlDataReader reader = command.ExecuteReader();

                    //on boucle sur les données

                    while (reader.Read())
                    {
                        userid = (int)reader.GetValue(0);
                    }

                    connexion.Close();
                    connexion.Open();
                    MySqlCommand command2 = new MySqlCommand("SELECT Public from Cles where ID_Profil ='" + userid + "'");

                    command2.Connection = connexion;

                    MySqlDataReader reader2 = command2.ExecuteReader();

                    //on boucle sur les données

                    string publickey = "";

                    while (reader2.Read())
                    {
                        publickey = reader2.GetValue(0).ToString();
                    }
                    mpz_t e = new mpz_t();
                    gmp_lib.mpz_init(e);
                    mpz_t n = new mpz_t();
                    gmp_lib.mpz_init(n);
                    Function.setMpz_t(e, publickey.Split(',')[0]);
                    Function.setMpz_t(n, publickey.Split(',')[1]);

                    connexion.Close();
                    //($"La clé publique est ( {n},{e} ) et la clé privé est ({n},{d})");

                    Random r = new Random();
                    int size = r.Next(1, words[0].Length);
                    List<byte> sentence = new List<byte>();
                    foreach (string line in lines)
                    {
                        sentence.AddRange(Encoding.ASCII.GetBytes(line));
                    }
                    byte[] ascii = sentence.ToArray();

                    int decount = 0;
                    string m = "";
                    foreach (byte byt in ascii)
                    {
                        m += byt;
                    }
                    int mlength = m.Length;
                    for (int i = size; i < mlength; i += size)
                    {
                        if (i + decount < m.Length)
                        {
                            m = m.Insert(i + decount, ";");
                            decount++;
                        }
                    }
                    string[] grouped = m.Split(';');
                    string temp = grouped.LastOrDefault();
                    while (temp.Length < size)
                    {
                        temp = temp.Insert(0, "0");
                    }
                    grouped[grouped.Length - 1] = temp;

                    string[] codedmessage = Function.encryption(grouped, e, n);
                    string message = string.Join(";", codedmessage);

                    //on ouvre la connexion

                    connexion.Open();
                    List<int> indexes = new List<int>();
                    int cpt = 0;
                    string objet = textBox1.Text;
                    foreach (char a in objet)
                    {
                        if (a.Equals('\''))
                        {
                            indexes.Add(cpt);
                        }
                        cpt++;
                    }
                    cpt = 0;
                    foreach (int index in indexes)
                    {
                        objet = objet.Insert(index + cpt, "\\");
                        cpt++;
                    }

                    MySqlCommand command3 = new MySqlCommand("insert into Messages (ID_Profil,ID_Destinataire,Sujet,Message,Date) values ('" + Form2.currentId + "','" + userid + "','" + objet + "','" + message + "','" + DateTime.Now.ToString("yyyyMMdd") + "')");
                    command3.Connection = connexion;
                    int result = command3.ExecuteNonQuery();
                    if (result > 0)
                        MessageBox.Show("Message envoyé.");

                    connexion.Close();
                }
                this.Close();
            }
            else if (checkedListBox1.SelectedItems == null)
            {
                MessageBox.Show("Veuillez selectionner un ou plusieurs utilisateur(s).");
            }
            else
            {
                MessageBox.Show("Veuillez remplir tous les champs.");
            }
        }
    }
}
